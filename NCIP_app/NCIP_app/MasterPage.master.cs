﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string isActiveDashboard { get; set; }
    public string isActivecases { get; set; }
    public string isActivematters { get; set; }
    public string isActivecalendar { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentPage = (string)Page.RouteData.Values["page"];
        switch (currentPage)
        {
            case "cases": isActivecases = "-active"; break;
            case "matter": isActivematters = "-active"; break;
        }
        if (Session["isLogin"] == null)
        {
            //Response.Redirect("~/login/loggedOut");
        }
    }
}
