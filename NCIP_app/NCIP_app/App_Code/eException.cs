﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;
using System.IO;

/// <summary>
/// Summary description for eException
/// </summary>
[Serializable]
public class eException : System.Exception
{
    private string m_exceptionString;

    public eException()
        : base()
    {
        m_exceptionString = null;
    }

    public eException(Exception ex)
        : base()
    {
        try
        {
            m_exceptionString = ex.Message;
            string folder = HttpContext.Current.Server.MapPath("~/Log/");
            string filename = "log.txt";
            string folderStructure = folder + filename;
            if (!File.Exists(folderStructure))
            {
                Directory.CreateDirectory(folder);
            }
            StreamWriter objwriter = File.AppendText(folderStructure);
            objwriter.WriteLine("-Date- : " + DateTime.Now);
            objwriter.WriteLine("-Error-: " + ex.Message);
            objwriter.WriteLine("-User- : " + general.getUser());
            objwriter.WriteLine("-URL-  : " + HttpContext.Current.Request.Url.AbsoluteUri.ToString());
            objwriter.WriteLine("___________________________________________________________________________");
            objwriter.Close();
        }
        catch (Exception x)
        {
            string filename = HttpContext.Current.Server.MapPath("~/Log/") + "log.txt";
            StreamWriter w;
            w = File.CreateText(filename);
            w.WriteLine("Text File Created on: " + DateTime.Now);
            w.WriteLine("____________________________________________________________________________________");
            w.Flush();
            w.Close();
        }
    }

    public eException(string exceptionString, Exception ex)
        : base(exceptionString, ex)
    {
        m_exceptionString = exceptionString;
    }

    public override string ToString()
    {
        return m_exceptionString;
    }
}