﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class SqlClientWrapper : general
{
    /// <summary>
    /// returns a json given an Sql Command, but you need to set .Con to your ConnectionString Name first.
    /// </summary>
    /// <param name="cmd">SqlCommand</param>
    /// <returns>json</returns>
    public static string getSet_JSON(SqlCommand cmd)
    {
        return DataSetToJSON(general.getSet(cmd));
    }

    /// <summary>
    /// Returns a Json given an sql statement and a client or connectionStringName defined on your webconfig
    /// </summary>
    /// <param name="sqlStatement">the sql statement</param>
    /// <param name="clientOrConnectionStringName">client or connectinstringname defined on your webconfig</param>
    /// <param name="isConnectionStringName">Specify if its a connectionstringname else dont set for it is a client </param>
    /// <returns>Json</returns>
    public static string getSet_JSON(string sqlStatement, string clientOrConnectionStringName, bool isConnectionStringName = false)
    {
        return DataSetToJSON(general.getSet(sqlStatement, clientOrConnectionStringName, isConnectionStringName));
    }

    /// <summary>
    /// returns a json given an sql command and a client or connectionStringName defined on your webconfig
    /// </summary>
    /// <param name="cmd">SqlCommand</param>
    /// <param name="clientOrConnectionStringName">client or connectinstringname defined on your webconfig</param>
    /// <param name="isConnectionStringName">Specify if its a connectionstringname else dont set for it is a client </param>
    /// <returns>json</returns>
    public static string getSet_JSON(SqlCommand cmd, string clientOrConnectionStringName, bool isConnectionStringName = false)
    {
        return DataSetToJSON(general.getSet(cmd, clientOrConnectionStringName, isConnectionStringName));
    }

    /// <summary>
    /// returns a json given an Sql statement but you need to set .Con to your ConnectionString Name first.
    /// </summary>
    /// <param name="sql">Sql Statement</param>
    /// <returns>JSON</returns>
    public static string getSet_JSON(string sql)
    {
        return DataSetToJSON(general.getSet(sql));
    }

    #region Stored Procedure 
    /// <summary>
    /// Create Stored Procedure Command, but you need to set .Con to your ConnectionString Name first.
    /// </summary>
    /// <param name="storedProcName">stored procedure name</param>
    /// <returns></returns>
    private static SqlCommand CreateStoredProcCommand(string storedProcName)
    {
        SqlCommand command = general.SqlConn().CreateCommand();
        command.CommandType = CommandType.StoredProcedure;
        command.CommandText = storedProcName;
        string cmdTimeout = "180";
        int timeout = -1;
        if (Int32.TryParse(cmdTimeout, out timeout))
        {
            command.CommandTimeout = timeout;
        }

        return command;
    }

    /// <summary>
    /// Get Parameters of the Stored Procedure
    /// </summary>
    /// <param name="storedProcedure"></param>
    /// <returns></returns>
    private static Dictionary<string, SqlDbType> GetSP_ParameterNameAndType(string storedProcedure)
    {
        var columns = new Dictionary<string, SqlDbType>();
        using (SqlCommand cmd = new SqlCommand(storedProcedure, general.SqlConn()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(cmd);
            int i = 0;
            foreach (SqlParameter p in cmd.Parameters)
            {
                if (p.ParameterName != "@RETURN_VALUE")
                {
                    SqlDbType type = p.SqlDbType;
                    bool isNullable = p.IsNullable;
                    columns.Add(p.ParameterName, type);
                    i++;
                }
            }
        }

        return columns;
    }

    /// <summary>
    /// Get Parameters of the Stored Procedure
    /// </summary>
    /// <param name="storedProcedure"></param>
    /// <returns></returns>
    public static Dictionary<string, object> GetSP_ParameterNames(string storedProcedure)
    {
        var columns = new Dictionary<string, object>();
        try
        {

            using (SqlCommand cmd = new SqlCommand(storedProcedure, general.SqlConn()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                foreach (SqlParameter p in cmd.Parameters)
                {
                    if (p.ParameterName != "@RETURN_VALUE")
                    {
                        columns.Add(p.ParameterName, null);
                    }
                }
            }

            return columns;
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
            //add to log file
            new eException(ex);
            return columns;
        }
    }

    /// <summary>
    /// Stored Procedure Fill Parameters with input values - handles the sqlDbType automatically from the db.
    /// </summary>
    /// <param name="storedProcedure">name of storedProcedure</param>
    /// <param name="input">a Dictionary of "parameter name" ex @MessageId and its "value" example 1</param>
    /// <returns></returns>
    public static SqlCommand SP_FillParameter(string storedProcedure, Dictionary<string, object> input)
    {
        //create store procedure
        var cmd = CreateStoredProcCommand(storedProcedure);
        try
        {
            //foreach parameters of the stored procedure
            foreach (var pmv in GetSP_ParameterNameAndType(storedProcedure))
            {
                //see if dictionary key matches a parameter name(pmv.Key)
                KeyValuePair<string, object> param = new KeyValuePair<string, object>();
                foreach (var in1 in input)
                {
                    if (in1.Key == pmv.Key.Trim())
                    {
                        param = in1;
                        break;
                    }
                }
                //if input key matches stored procedure parameter name.
                if (!string.IsNullOrEmpty(param.Key))
                {
                    //add to command parameters
                    cmd.Parameters.Add(pmv.Key, pmv.Value).Value = param.Value;
                }
            }
            return cmd;
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
            //add to log file
            new eException(ex);
            return cmd;
        }
    }
#endregion

    /// <summary>
    /// DataSetToJSON
    /// </summary>
    /// <param name="DataSet">ds</param>
    /// <returns>bool</returns>
    static string DataSetToJSON(DataSet ds)
    {
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                return general.DStoJSON(ds);
            }
        }
        return "";
    }
}
