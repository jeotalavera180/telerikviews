﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head id="Head1" runat="server">
    <title>National Commission on Indigenous Peoples</title>

    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css">

    <link rel="stylesheet" type="text/css" href="css/footable-0.1.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-notify.css">
    <link rel="stylesheet" type="text/css" href="css/opentides-3.0.1.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-modal.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-dialog.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>	
    <script type="text/javascript" src="js/jquery.deserialize.js"></script>	
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/footable-0.1.js"></script>
    <script type="text/javascript" src="js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>
    <script type="text/javascript" src="js/select2.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="js/bootstrap-dialog.js"></script>
    <script type="text/javascript" src="js/opentides-3.0.1.js"></script>
    <script type="text/javascript" src="js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="js/bootstrap-modalmanager.js"></script>
    <script type="text/javascript" src="js/jquery.form.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 </head>
<body class="anonymous-page" screen_capture_injected="true" cz-shortcut-listen="true">

    	
    	<div class="notifications top-right"></div>
		<div id="wrap">
	
			<div id="hd" class="navbar login-nav navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
                    	<div class="brand-wrapper">
                            <a class="brand"><img class="logo" src="img/logo.png"></a>
                            <div class="brand-name">
                                <h3>National Commission on Indigenous Peoples</h3>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			<div class="main container">
			
			<div id="bd" class="container">

				<div class="wallpaper"></div>
				<div class="wallpaper-grid"></div>
                
                <div class="cms-title">
                	<h3>Case Management System</h3>
                </div>

				<div class="form-login modal">
					<div class="modal-body">
						
						<div class="row-fluid">
							
								
								<h4>Log In</h4>
								<hr>
								
								<div class="control-group">
									<input id="ats_username" name="ats_username" class="input-block-level" type="text" name="" placeholder="Username" autofocus="autofocus">
								</div>
								<div class="control-group">
									<input id="ats_password" name="ats_password" class="input-block-level" type="password" name="" placeholder="Password">
								</div>
								
								<!--<div class="control-group">
									<label class="checkbox">
										<input type="checkbox" checked="">
										<small>Keep me logged in</small>
									</label>
								</div>-->
								
								<input type="button" class="btn btn-info btn-block btn-large" id="loginBtn" name="loginBtn" value="Log In">
								
							<!--<div class="span6 alternative-logins pagination-centered">
								<div class="control-group"><small>Log in if your account is connected with Facebook, Twitter or Google+</small></div>
								<div class="control-group">
									<button class="btn btn-primary btn-block">
									<i class="icon-facebook-sign"></i>Log in with Facebook</button>
								</div>
								<div class="control-group">
									<button class="btn btn-info btn-block">
									<i class="icon-twitter-sign"></i>Log in with Twitter</button>
								</div>
								<div class="control-group">
									<button class="btn btn-danger btn-block">
									<i class="icon-google-plus-sign"></i>Log in with Google</button>
								</div>
							</div>-->
						</div>
					</div>
					<!--<div class="modal-footer">
						<small class="pull-left">Don't have an account? <a class="show-signup">Sign up now</a></small>
						<small class="pull-right"><a>Forgot password?</a></small>
					</div>-->
				</div>

			</div>
            <!--<p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>-->
		
		<div id="push"></div>
		
	</div>
	
	<div id="ft">
      <div class="container">
        <p class="muted credit">
        	<small>
        		© National Commission on Indigenous Peoples |&nbsp; All Rights Reserved.
        	</small>
        </p>
      </div>
    </div>

<!-- Custom JS -->
<script type="text/javascript" src="js/mainScript.js"></script>

</body>
</html>
