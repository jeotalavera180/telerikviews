﻿
var pageNum = 1;
jQuery(document).ready(function ($) {


    $('#sidebar-arrow').click(function () {

        if ($(this).find('i').hasClass('icon-chevron-right')) {
            $('.sidebar-wrapper').animate({ left: 0 }, function () {
                $(this).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
            });
        } else {
            $('.sidebar-wrapper').animate({ left: -93 }, function () {
                $(this).find('i').addClass('icon-chevron-right').removeClass('icon-chevron-left');
            });
        }
    });

    $("#opener").click(function () {
        TINY.box.show({
            html: private_message_form,
            width: 350,
            height: 288,
            fixed: true
        });
    });
});