﻿function show(edit_case_details) {
    var element = document.getElementById(edit_case_details);
    element.style.display = "";
}
function hide_edit_case_details(edit_case_details) {
    var element = document.getElementById(edit_case_details);
    element.style.display = "none";
}
function hide_edit_tags(edit_tags) {
    var element = document.getElementById(edit_tags);
    element.style.display = "none";

    var element = document.getElementById(edit_label);
    element.style.display = "none";
}
 function show(match_found) {
    var element = document.getElementById(match_found);
    element.style.display = "";
}
function hide(match_found) {
    var element = document.getElementById(match_found);
    element.style.display = "none";
}
function show(advanced_search) {
    var element = document.getElementById(advanced_search);
    element.style.display = "";
}
function hide(advanced_search) {
    var element = document.getElementById(advanced_search);
    element.style.display = "none";
}
function show(advanced) {
    var element = document.getElementById(advanced);
    element.style.display = "";
}
function hide(advanced) {
    var element = document.getElementById(advanced);
    element.style.display = "none";
}
function show(basic) {
    var element = document.getElementById(basic);
    element.style.display = "";
}
function hide(basic) {
    var element = document.getElementById(basic);
    element.style.display = "none";
}
function show(search_box) {
    var element = document.getElementById(search_box);
    element.style.display = "";
}
function hide(search_box) {
    var element = document.getElementById(search_box);
    element.style.display = "none";
}
 function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
function errorMsg(msg) {
    BootstrapDialog.show({
        title: "Error",
        message: msg,
        type: BootstrapDialog.TYPE_DANGER,
        buttons: [{
            label: 'Close',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}
function successMsg(msg) {
    BootstrapDialog.show({
        title: "Info",
        message: msg,
        type: BootstrapDialog.TYPE_INFO,
        buttons: [{
            label: 'Close',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

var credentials = {
    username: 0,
    password: 0,
    userid: 0,
    access: 0
};

jQuery(document).ready(function ($) {

    if (jQuery("#hmsg").length == 1) {
        var popupMsg = jQuery("#hmsg").val();
        if (popupMsg != "") {
            successMsg(popupMsg);
        }
    }
    $('#sidebar-arrow').click(function () {

        if ($(this).find('i').hasClass('icon-chevron-right')) {
            $('.sidebar-wrapper').animate({ left: 0 }, function () {
                $(this).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
            });
        } else {
            $('.sidebar-wrapper').animate({ left: -93 }, function () {
                $(this).find('i').addClass('icon-chevron-right').removeClass('icon-chevron-left');
            });
        }
    });

    //login button click
    jQuery('#loginBtn').click(function (e) {
        e.preventDefault();

        credentials.username = jQuery("#ats_username").val();
        credentials.password = jQuery("#ats_password").val();

        if (validateEmail(credentials.username)) {
            jQuery.ajax({
                type: 'POST',
                url: "http://localhost:58496/NCIP_prototype/Service/Service_json.aspx",
                data: { action: 'userlogin', data: credentials },
                success: function (data) {
                    window.location.href = 'index.aspx/Home';
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    errorMsg("Invalid Credentials, please try again");
                    jQuery("#ats_username").focus();
                }
            });
        }
        else {
            errorMsg("Invalid Email Address. please try again");
        }
    });
});