﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_items.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_items" %>


<telerik:RadPanelItem Text="Items" Expanded="true">
    <contenttemplate>
                <ul>
                    <li>
                        <telerik:RadGrid ID="grid_items" runat="server" OnNeedDataSource="grid_items_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_items_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Item"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="category"
                                         HeaderText="Category"
                                        SortExpression="category" UniqueName="category">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="name"
                                        HeaderText="Name"
                                        SortExpression="name" UniqueName="name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="unit"
                                     HeaderText="Unit"
                                        SortExpression="unit" UniqueName="unit">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="type"
                                     HeaderText="Type"
                                        SortExpression="type" UniqueName="type">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />
                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />
                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <ul>
                                                <li>
                                                <label>Category</label>
                                                </li>
                                                <li>

                                                    <telerik:RadComboBox ID="txt_category" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Unit</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_unit" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Name</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Item Type</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_itemType" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Item Code</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_itemCode" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                    <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                                    <input type="checkbox" name="ckbx_trackSales" value="">Track as sales<br>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" />
                                                </li>
                                        </ul>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                </ul>
            </contenttemplate>
</telerik:RadPanelItem>