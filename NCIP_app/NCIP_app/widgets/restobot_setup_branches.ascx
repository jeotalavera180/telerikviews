﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_branches.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_branches" %>

<h1>Branches</h1>
<telerik:RadGrid ID="grid_branches" runat="server" OnNeedDataSource="grid_branches_NeedDataSource"
    AllowPaging="True" AllowSorting="True" OnItemCommand="grid_branches_ItemCommand"
    CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
    PageSize="10">

    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
        DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add brand"
            ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
        <Columns>
            <telerik:GridBoundColumn DataField="id" Display="False"
                UniqueName="id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="name"
                HeaderText="NAME"
                SortExpression="name" UniqueName="name">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="mobile"
                HeaderText="MOBILE"
                SortExpression="mobile" UniqueName="mobile">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="landline"
                HeaderText="LANDLINE"
                SortExpression="landline" UniqueName="landline">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn
                ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                ButtonCssClass="icon-edit" />
            <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                ButtonCssClass="icon-trash" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Address1</th>
                        <th>Address2</th>
                        <th>City</th>
                        <th>Zip</th>
                        <th>Email address</th>
                        <th>Landline no</th>
                        <th>Fax no</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_address1" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_address2" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_city" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_zip" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_email" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_landline" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="txt_fax" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Button ID="btn_Save" runat="server" Text="Save" CommandName="newInsert" />
                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="cancel" />
                        </td>
                    </tr>
                </table>
            </FormTemplate>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
    <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
    <FilterMenu EnableImageSprites="False"></FilterMenu>

</telerik:RadGrid>

