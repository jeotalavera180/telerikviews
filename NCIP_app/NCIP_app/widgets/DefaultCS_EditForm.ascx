﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultCS_EditForm.ascx.cs" Inherits="NCIP_app.widgets.DefaultCS_EditForm" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<h4><%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %></h4>
<ul>
    <li>
        <label>Username</label>
        <telerik:RadComboBox ID="txt_username" runat="server" ShowToggleImage="false"
            EmptyMessage="" AllowCustomText="True" Text='<%# DataBinder.Eval(Container,"DataItem.username") %>'>
        </telerik:RadComboBox>
    </li>
    <li>
        <label>Password</label>
        <telerik:RadComboBox ID="txt_password" runat="server" ShowToggleImage="false"
            EmptyMessage="" AllowCustomText="True" Text='<%# DataBinder.Eval(Container,"DataItem.password") %>'>
        </telerik:RadComboBox>
    </li>
    <li>
        <table>
            <tr>
                <td>
                    <telerik:RadButton ID="ckbx_isActive" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                        AutoPostBack="false" Text="Is Active">
                    </telerik:RadButton>
                </td>
                <td>
                    <telerik:RadButton ID="ckbx_trackAsSales" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                        AutoPostBack="false" Text="Track as Sales">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
    </li>
    <li>
        <asp:Button ID="btn_Save" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
            runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "addCategory" : "updateCategory" %>'></asp:Button>&nbsp;
                                <asp:Button ID="btn_Cancel" Text="Cancel" runat="server" CausesValidation="False"
                                    CommandName="cancel"></asp:Button>
    </li>
</ul>









