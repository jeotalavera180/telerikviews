﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_suppliers.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_suppliers" %>

<h1>Suppliers</h1>
<telerik:RadGrid ID="grid_suppliers" runat="server" OnNeedDataSource="grid_suppliers_NeedDataSource"
    AllowPaging="True" AllowSorting="True" OnItemCommand="grid_suppliers_ItemCommand"
    CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
    PageSize="10">

    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
    </ClientSettings>

    <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
        DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Supplier"
            ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
        <Columns>
            <telerik:GridBoundColumn DataField="id" Display="False"
                UniqueName="id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="name"
                HeaderText="NAME"
                SortExpression="name" UniqueName="name">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="contact"
                HeaderText="CONTACT"
                SortExpression="contact" UniqueName="contact">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="mobile"
                HeaderText="MOBILE #"
                SortExpression="mobile" UniqueName="mobile">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn
                ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                ButtonCssClass="icon-edit" />
            <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                ButtonCssClass="icon-trash" />
        </Columns>

        <EditFormSettings EditFormType="Template">
            <FormTemplate>
                <center>
                        <div>
                            <label>Assigned to</label>
                            <telerik:RadComboBox ID="txt_assignedTo" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Name</label>
                            <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Contact person</label>
                            <telerik:RadComboBox ID="txt_contactPerson" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Contact Title</label>
                            <telerik:RadComboBox ID="txt_contactTitle" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>TIN</label>
                            <telerik:RadComboBox ID="txt_tin" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Landline no</label>
                            <telerik:RadComboBox ID="txt_landline" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Mobile no</label>
                            <telerik:RadComboBox ID="txt_mobile" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <label>Fax no</label>
                            <telerik:RadComboBox ID="txt_fax" runat="server" ShowToggleImage="false"
                                EmptyMessage="Choose" AllowCustomText="True">
                            </telerik:RadComboBox>
                        </div>
                        <div>
                            <asp:Button ID="btn_Save" runat="server" Text="Save" CommandName="newInsert" />
                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="cancel" />
                        </div>
                </center>
            </FormTemplate>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
    <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
    <FilterMenu EnableImageSprites="False"></FilterMenu>

</telerik:RadGrid>