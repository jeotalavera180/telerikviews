﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace NCIP_app.widgets
{
    public partial class test_formtemplateupdate : System.Web.UI.UserControl
    {
        //protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        //{
        //    if (e.Exception != null)
        //    {

        //        DisplayMessage(true, "Employee " + e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EmployeeID"] + " cannot be updated. Reason: " + e.Exception.Message);
        //    }
        //    else
        //    {
        //        RadComboBox editusername = (RadComboBox)e.Item.FindControl("TextBox7");
        //        RadComboBox editpassword = (RadComboBox)e.Item.FindControl("TextBox8");
        //        var edit_uname = editusername.Text;
        //        var edit_pword = editpassword.Text;
        //        e.KeepInEditMode = true;
        //        e.ExceptionHandled = true;
        //        DisplayMessage(false, "Employee " + e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EmployeeID"] + " updated"+edit_pword+edit_uname);
        //    }
        //}

        //protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e)
        //{
        //    if (e.Exception != null)
        //    {
        //        e.ExceptionHandled = true;
        //        e.KeepInInsertMode = true;
        //        DisplayMessage(true, "Employee cannot be inserted. Reason: " + e.Exception.Message);
        //    }
        //    else
        //    {
        //        DisplayMessage(false, "Employee inserted");
        //    }
        //}

        //protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        //{
        //    if (e.Exception != null)
        //    {
        //        e.ExceptionHandled = true;
        //        DisplayMessage(true, "Employee " + e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EmployeeID"] + " cannot be deleted. Reason: " + e.Exception.Message);
        //    }
        //    else
        //    {
        //        DisplayMessage(false, "Employee " + e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EmployeeID"] + " deleted");
        //    }
        //}

        //private void DisplayMessage(bool isError, string text)
        //{
        //    Label label = (isError) ? this.Label1 : this.Label2;
        //    label.Text = text;
        //}

        //protected void Page_Load(object sender, System.EventArgs e)
        //{
        //}

        //protected void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        //{
        //    if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
        //    {
        //        GridEditCommandColumn editColumn = (GridEditCommandColumn)RadGrid1.MasterTableView.GetColumn("EditCommandColumn");
        //        editColumn.Visible = false;
        //    }
        //    else if (e.CommandName == RadGrid.RebindGridCommandName && e.Item.OwnerTableView.IsItemInserted)
        //    {
        //        e.Canceled = true;
        //    }
        //    else
        //    {
        //        GridEditCommandColumn editColumn = (GridEditCommandColumn)RadGrid1.MasterTableView.GetColumn("EditCommandColumn");
        //        if (!editColumn.Visible)
        //            editColumn.Visible = true;
        //    }
        //}
        //protected void RadGrid1_PreRender(object sender, EventArgs e)
        //{
        //    if (!Page.IsPostBack)
        //    {
        //        //RadGrid1.EditIndexes.Add(0);
        //        //RadGrid1.Rebind();
        //    }
        //}

        //protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    if (!e.IsFromDetailTable)
        //    {
        //        RadGrid1.DataSource = general.getSet("select * from t_User");//GetDataTable("SELECT * FROM t_Users");
        //    }
        //    else
        //    {

        //    }
        //}

        //private NorthwindReadWriteEntities _dataContext;

        //protected NorthwindReadWriteEntities DbContext
        //{
        //    get
        //    {
        //        if (_dataContext == null)
        //        {
        //            _dataContext = new NorthwindReadWriteEntities();
        //        }
        //        return _dataContext;
        //    }
        //}

        public override void Dispose()
        {
            //if (_dataContext != null)
            //{
            //    _dataContext.Dispose();
            //}
            base.Dispose();
        }

        protected void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = general.getSet("select * from t_User");
        }

        protected void RadGrid1_UpdateCommand(object source, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var productId = (int)editableItem.GetDataKeyValue("ProductID");

            //retrive entity form the Db
            //var product = DbContext.Products.Where(n => n.ProductID == productId).FirstOrDefault();
            //if (product != null)
            {
                //update entity's state
               // editableItem.UpdateValues(product);

                try
                {
                    //save chanages to Db
                   // DbContext.SaveChanges();
                }
                catch (System.Exception)
                {
                    ShowErrorMessage();
                }
            }
        }

        /// <summary>
        /// Shows a <see cref="RadWindow"/> alert if an error occurs
        /// </summary>
        private void ShowErrorMessage()
        {
            RadAjaxManager1.ResponseScripts.Add(string.Format("window.radalert(\"Please enter valid data!\")"));
        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && (e.Item.IsInEditMode))
            {
                GridEditableItem editableItem = (GridEditableItem)e.Item;
                SetupInputManager(editableItem);
            }
        }

        private void SetupInputManager(GridEditableItem editableItem)
        {
            // style and set ProductName column's textbox as required
            var textBox =
                ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("username")).TextBoxControl;


            InputSetting inputSetting = RadInputManager1.GetSettingByBehaviorID("TextBoxSetting1");
            inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
            inputSetting.InitializeOnClient = true;
            inputSetting.Validation.IsRequired = true;

            // style UnitPrice column's textbox 
            textBox =
                ((GridTextBoxColumnEditor)editableItem.EditManager.GetColumnEditor("password")).TextBoxControl;

            inputSetting = RadInputManager1.GetSettingByBehaviorID("NumericTextBoxSetting1");
            inputSetting.InitializeOnClient = true;
            inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));


            //inputSetting = RadInputManager1.GetSettingByBehaviorID("NumericTextBoxSetting2");
            inputSetting.InitializeOnClient = true;
            inputSetting.TargetControls.Add(new TargetInput(textBox.UniqueID, true));
        }

        protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            //create new entity
            //var product = new Model.ReadWrite.Product();
            ////populate its properties
            //Hashtable values = new Hashtable();
            //editableItem.ExtractValues(values);
            //product.ProductName = (string)values["ProductName"];
            //if (values["UnitsInStock"] != null)
            //{
            //    product.UnitsInStock = short.Parse(values["UnitsInStock"].ToString());
            //}
            //if (values["UnitPrice"] != null)
            //{
            //    product.UnitPrice = decimal.Parse(values["UnitPrice"].ToString());
            //}

            //DbContext.AddToProducts(product);
            //try
            //{
            //    //save chanages to Db
            //    DbContext.SaveChanges();
            //}
            //catch (System.Exception)
            //{
            //    ShowErrorMessage();
            //}
        }

        protected void RadGrid1_DeleteCommand(object source, GridCommandEventArgs e)
        {
            var productId = (int)((GridDataItem)e.Item).GetDataKeyValue("ProductID");

            //retrive entity form the Db
            //var product = DbContext.Products.Where(n => n.ProductID == productId).FirstOrDefault();
            //if (product != null)
            //{
            //    //add the product for deletion
            //    DbContext.Products.DeleteObject(product);
            //    try
            //    {
            //        //save chanages to Db
            //        DbContext.SaveChanges();
            //    }
            //    catch (System.Exception)
            //    {
            //        ShowErrorMessage();
            //    }
            //}
        }
    }
}