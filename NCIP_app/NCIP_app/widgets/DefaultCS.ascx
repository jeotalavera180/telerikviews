﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultCS.ascx.cs" Inherits="NCIP_app.widgets.DefaultCS" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<body>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }

            function onPopUpShowing(sender, args) {
                args.get_popUp().className += " popUpEditForm";
            }

        </script>
    </telerik:RadCodeBlock>
    <div class="demo-container no-bg">
        <%--<telerik:RadFormDecorator runat="server" DecorationZoneID="demo" EnableRoundedCorners="false" DecoratedControls="All" />--%>
        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False"
            CssClass="RadGrid" GridLines="None" Skin="MetroTouch" OnItemCreated="RadGrid1_ItemCreated"
            PageSize="7" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
            OnDetailTableDataBind="RadGrid1_DetailTableDataBind" OnNeedDataSource="RadGrid1_NeedDataSource"
            OnItemCommand="RadGrid1_ItemCommand" OnPreRender="RadGrid1_PreRender">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <%--EditMode="PopUp"--%>
            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                DataKeyNames="id" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Category"
                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="id" HeaderText="Id" HeaderButtonType="TextButton"
                        DataField="id" Visible="False" ReadOnly="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="username" HeaderText="Username" HeaderButtonType="TextButton"
                        DataField="username" MaxLength="50">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="password" HeaderText="Password" HeaderButtonType="TextButton"
                        DataField="password" MaxLength="50">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn
                        ConfirmTitle="Edit" CommandName="Edit" CommandArgument="id"
                        ButtonCssClass="icon-edit" ItemStyle-Width="5px" />
                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="delete" CommandName="deleteCategory" CommandArgument="id"
                        ButtonCssClass="icon-trash" ItemStyle-Width="5px" />
                </Columns>
                <EditFormSettings EditFormType="Template" EditColumn-CancelImageUrl="../img/exit.gif">
                    <%--<PopUpSettings Modal="true" ZIndex="100010" CloseButtonToolTip="Close" ShowCaptionInEditForm="true" />--%>
                    <FormTemplate>
                        <center>
                            <h4><%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %></h4>
                        <ul>
                            <li>
                                <label>Username</label>
                                <telerik:RadComboBox ID="txt_username" runat="server" ShowToggleImage="false"
                                    EmptyMessage="" AllowCustomText="True" Text='<%# Bind("username") %>'>
                                </telerik:RadComboBox>
                            </li>
                            <li>
                                <label>Password</label>
                                <telerik:RadComboBox ID="txt_password" runat="server" ShowToggleImage="false"
                                    EmptyMessage="" AllowCustomText="True" Text='<%# Bind("password") %>'>
                                </telerik:RadComboBox>
                            </li>
                            <li>
                               <telerik:RadGrid runat="server" AutoGenerateColumns="False" ID="RadGrid_Test"
                                    CssClass="RadGrid" GridLines="None" Skin="MetroTouch" AllowPaging="True"
                                    PageSize="7" AllowSorting="True" AllowMultiRowSelection="False" OnItemCreated="RadGrid_Test_ItemCreated"
                                    OnDetailTableDataBind="RadGrid_Test_DetailTableDataBind" OnNeedDataSource="RadGrid_Test_NeedDataSource"
                                    OnItemCommand="RadGrid_Test_ItemCommand" OnPreRender="RadGrid_Test_PreRender" >
                                   <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                    DataKeyNames="id" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                       <Columns>
                                           <telerik:GridBoundColumn SortExpression="id" HeaderText="Id" HeaderButtonType="TextButton"
                                                DataField="id" Visible="False" ReadOnly="true">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn SortExpression="username" HeaderText="Username" HeaderButtonType="TextButton"
                                                DataField="username" MaxLength="50">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn SortExpression="password" HeaderText="Password" HeaderButtonType="TextButton"
                                                DataField="password" MaxLength="50">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn
                                                ConfirmTitle="Edit" CommandName="Edit" CommandArgument="id"
                                                ButtonCssClass="icon-edit" ItemStyle-Width="5px" />
                                            <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                                ConfirmTitle="delete" CommandName="deleteCategory" CommandArgument="id"
                                                ButtonCssClass="icon-trash" ItemStyle-Width="5px" />
                                       </Columns>
                                       <EditFormSettings EditFormType="Template">
                                            <%--<PopUpSettings Modal="true" ZIndex="100010" CloseButtonToolTip="Close" ShowCaptionInEditForm="true" />--%>
                                            <FormTemplate>
                                                <center>
                                                    <h4><%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %></h4>
                                                <ul>
                                                    <li>
                                                        <label>Username</label>
                                                        <telerik:RadComboBox ID="txt_username" runat="server" ShowToggleImage="false"
                                                            EmptyMessage="" AllowCustomText="True" Text='<%# Bind("username") %>'>
                                                        </telerik:RadComboBox>
                                                    </li>
                                                    <li>
                                                        <label>Password</label>
                                                        <telerik:RadComboBox ID="txt_password" runat="server" ShowToggleImage="false"
                                                            EmptyMessage="" AllowCustomText="True" Text='<%# Bind("password") %>'>
                                                        </telerik:RadComboBox>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <telerik:RadButton ID="ckbx_isActive" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                                        AutoPostBack="false" Text="Is Active">
                                                                    </telerik:RadButton>
                                                                </td>
                                                                <td>
                                                                    <telerik:RadButton ID="ckbx_trackAsSales" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                                        AutoPostBack="false" Text="Track as Sales">
                                                                    </telerik:RadButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <asp:Button ID="btn_Save" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                runat="server"  CommandName='<%# (Container is GridEditFormInsertItem) ? "addCategory" : "updateCategory" %>'></asp:Button>&nbsp;
                                                        <asp:Button ID="btn_Cancel" Text="Cancel" runat="server" CausesValidation="False"
                                                            CommandName="cancel"></asp:Button>
                                                    </li>
                                                </ul>
                                            </center>
                                            </FormTemplate>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                        </EditFormSettings>
                                   </MasterTableView>
                               </telerik:RadGrid>
                            </li>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <telerik:RadButton ID="ckbx_isActive" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                AutoPostBack="false" Text="Is Active">
                                            </telerik:RadButton>
                                        </td>
                                        <td>
                                            <telerik:RadButton ID="ckbx_trackAsSales" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton"
                                                AutoPostBack="false" Text="Track as Sales">
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                                <asp:Button ID="btn_Save" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                        runat="server"  CommandName='<%# (Container is GridEditFormInsertItem) ? "addCategory" : "updateCategory" %>'></asp:Button>&nbsp;
                                <asp:Button ID="btn_Cancel" Text="Cancel" runat="server" CausesValidation="False"
                                    CommandName="cancel"></asp:Button>
                            </li>
                        </ul>
                    </center>
                    </FormTemplate>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                </EditFormSettings>
                <DetailTables>
                    <telerik:GridTableView EnableNoRecordsTemplate="true" NoDetailRecordsText=""
                        DataKeyNames="id"
                        Name="sub-categories" Width="100%" GridLines="None" CssClass="something" PageSize="7">
                        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Sub-Category"
                            ShowRefreshButton="true"></CommandItemSettings>
                        <NoRecordsTemplate>
                            <center>
                                <asp:LinkButton Text="Add Sub-Category" runat="server" CommandName="InitInsert" />
                            </center>
                        </NoRecordsTemplate>
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="id" HeaderText="Id" UniqueName="subCategoryId"
                                DataField="id" Visible="False" ReadOnly="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="title" HeaderText="Title" HeaderButtonType="TextButton"
                                DataField="title" UniqueName="title" MaxLength="20" DataType="System.String">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="description" HeaderText="Description"
                                HeaderButtonType="TextButton" MaxLength="50" DataType="System.String" DataField="description">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="tags" HeaderText="Tags"
                                DataField="tag" DataType="System.String" MaxLength="50" Visible="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn
                                ConfirmTitle="Edit" CommandName="Edit" CommandArgument="id"
                                ButtonCssClass="icon-edit" ItemStyle-Width="5px" />
                            <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                ButtonCssClass="icon-trash" ItemStyle-Width="5px" HeaderTooltip="Delete" />
                        </Columns>
                        <%--        <EditFormSettings EditFormType="WebUserControl" UserControlName="DefaultCS_EditForm.ascx">
                            <EditColumn UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>--%>
                        <EditFormSettings EditFormType="Template">
                            <FormTemplate>
                                <center>
                                        <h4><%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %></h4>
                                    <ul>
                                        <li>
                                            <label>Title</label>
                                            <telerik:RadComboBox ID="txt_title" runat="server" ShowToggleImage="false"
                                                EmptyMessage="" AllowCustomText="True" Text='<%# Bind("title") %>'>
                                            </telerik:RadComboBox>
                                        </li>
                                        <li>
                                            <label>Description</label>
                                            <telerik:RadComboBox ID="txt_description" runat="server" ShowToggleImage="false"
                                                EmptyMessage="" AllowCustomText="True" Text='<%# Bind("description") %>'>
                                            </telerik:RadComboBox>
                                        </li>
                                        <li>
                                            <div  >
                                            <label runat="server" visible='<%# (Container is GridEditFormInsertItem) ? false : true %>'>Tags</label>
                                            <telerik:RadComboBox ID="txt_tags" runat="server" ShowToggleImage="false"
                                                EmptyMessage="" AllowCustomText="True" Text='<%# Bind("tag") %>'
                                                Visible='<%# (Container is GridEditFormInsertItem) ? false : true %>'>
                                            </telerik:RadComboBox>
                                            </div>
                                        </li>
                                        <li>
                                            <asp:Button ID="btn_Save" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                    runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "addSubCategory" : "updateSubCategory" %>'></asp:Button>&nbsp;
                                            <asp:Button ID="btn_Cancel" Text="Cancel" runat="server" CausesValidation="False"
                                                CommandName="cancel"></asp:Button>
                                        </li>
                                    </ul>
                                </center>
                            </FormTemplate>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                        </EditFormSettings>
                    </telerik:GridTableView>
                </DetailTables>
            </MasterTableView>
            <ClientSettings>
                <Selecting AllowRowSelect="true" />
                <ClientEvents OnRowDblClick="RowDblClick" OnPopUpShowing="onPopUpShowing" />
            </ClientSettings>
        </telerik:RadGrid>
    </div>
</body>
