﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_units.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_units" %>


<telerik:RadPanelItem Text="Purchases" Expanded="true">
    <contenttemplate>
                <ul>
                    <li>
                        <telerik:RadGrid ID="grid_units" runat="server" OnNeedDataSource="grid_units_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_units_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Unit"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="name"
                                         HeaderText="NAME"
                                        SortExpression="name" UniqueName="name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="symbol"
                                        HeaderText="SYMBOL"
                                        SortExpression="symbol" UniqueName="symbol">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="remark"
                                        FilterControlAltText="Filter branch column" HeaderText="REMARKS"
                                        SortExpression="remark" UniqueName="remark">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />
                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />
                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <ul>
                                                <li>
                                                <label>Name</label>
                                                </li>
                                                <li>

                                                    <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Symbol</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_symbol" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Remark</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_remark" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                    <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                                    <input type="checkbox" name="ckbx_trackSales" value="Bike">Track as sales<br>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" />
                                                </li>
                                        </ul>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                </ul>
            </contenttemplate>
</telerik:RadPanelItem>