﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_settlements.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_settlements" %>


<telerik:RadPanelItem Text="Settlements" Expanded="true">
    <contenttemplate>
                <ul>
                    <li>
                        <telerik:RadGrid ID="grid_settlements" runat="server" OnNeedDataSource="grid_settlements_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_settlements_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Settlement"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="name"
                                         HeaderText="NAME"
                                        SortExpression="name" UniqueName="name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="description"
                                        HeaderText="DESCRIPTION"
                                        SortExpression="description" UniqueName="description">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="complimentary"
                                     HeaderText="COMPLIMENTARY"
                                        SortExpression="complimentary" UniqueName="complimentary">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="active"
                                     HeaderText="ACTIVE"
                                        SortExpression="active" UniqueName="active">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />
                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />
                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <ul>
                                                <li>
                                                <label>Name</label>
                                                </li>
                                                <li>

                                                    <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Description</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_description" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                
                                                <li>
                                                    
                                                    <input type="checkbox" name="ckbx_isComplimentary" value="Bike">Is Complimentary?<br>
                                                    <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" />
                                                </li>
                                        </ul>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                </ul>
            </contenttemplate>
</telerik:RadPanelItem>