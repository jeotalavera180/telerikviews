﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace NCIP_app.widgets
{
    public partial class restobot_setup_categories : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlClientWrapper.Con = "NCIPConnectionString";
        }

        protected void grid_categories_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            //
            grid_categories.DataSource = SqlClientWrapper.getSet("select * from t_User");

            //if empty String.Empty;
        }

        protected void grid_categories_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string primaryKey = "";
            try
            {
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex]["id"].Text;
            }
            catch (Exception)
            {
            }

            if (e.CommandName == "RowClick" || e.CommandName == "ExpandCollapse")
            {
                bool lastState = e.Item.Expanded;

                if (e.CommandName == "ExpandCollapse")
                {
                    lastState = !lastState;
                }

                CollapseAllRows();
                e.Item.Expanded = !lastState;
            }

            switch (e.CommandName)
            {
                case "InitInsert": break;
                case "update": break;
                case "delete":
                    /*SqlCommand cmd = new SqlCommand("UPDATE t_matters set status='DELETED' where id=@id");
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = primaryKey;
                    general.performActionNoTrans(cmd);
                    RadGrid1.Rebind();*/
                    break;
                case "newInsert":
                    //RadComboBox username = (RadComboBox)e.Item.FindControl("txt_username");
                    //RadComboBox password = (RadComboBox)e.Item.FindControl("txt_password");
                    //var _uname = username.Text;
                    //var _pword = password.Text;

                    //SqlCommand cmd = new SqlCommand("insert into t_user (username,password,department_id) values (@username,@password,@department_id)");
                    //cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = _uname;
                    //cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = _pword;
                    //cmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    //bool isSaved = general.performAction(cmd);
                    //if (isSaved)
                    //{
                    //    grid_branches.Rebind();
                    //}
                    //else
                    //{
                    //    //oh no
                    //}
                    grid_categories.MasterTableView.ClearEditItems();//to clear the EditMode 
                    grid_categories.EditIndexes.Clear();
                    grid_categories.MasterTableView.IsItemInserted = false;
                    grid_categories.Rebind();
                    break;
                default: break;
            }

        }



        private void CollapseAllRows()
        {
            foreach (GridItem item in grid_categories.MasterTableView.Items)
            {
                item.Expanded = false;
            }
        }

        protected void grid_subCategories_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            
            //
            grid_categories.DataSource = SqlClientWrapper.getSet("select * from t_User");
           
            //if empty String.Empty;
        }

        protected void grid_subCategories_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string primaryKey = "";
            try
            {
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex]["id"].Text;
            }
            catch (Exception)
            {
            }

            if (e.CommandName == "RowClick" || e.CommandName == "ExpandCollapse")
            {
                bool lastState = e.Item.Expanded;

                if (e.CommandName == "ExpandCollapse")
                {
                    lastState = !lastState;
                }

                CollapseAllRows();
                e.Item.Expanded = !lastState;
            }

            switch (e.CommandName)
            {
                case "InitInsert": break;
                case "update": break;
                case "delete":
                    /*SqlCommand cmd = new SqlCommand("UPDATE t_matters set status='DELETED' where id=@id");
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = primaryKey;
                    general.performActionNoTrans(cmd);
                    RadGrid1.Rebind();*/
                    break;
                case "newInsert":
                    //RadComboBox username = (RadComboBox)e.Item.FindControl("txt_username");
                    //RadComboBox password = (RadComboBox)e.Item.FindControl("txt_password");
                    //var _uname = username.Text;
                    //var _pword = password.Text;

                    //SqlCommand cmd = new SqlCommand("insert into t_user (username,password,department_id) values (@username,@password,@department_id)");
                    //cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = _uname;
                    //cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = _pword;
                    //cmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    //bool isSaved = general.performAction(cmd);
                    //if (isSaved)
                    //{
                    //    grid_branches.Rebind();
                    //}
                    //else
                    //{
                    //    //oh no
                    //}
                    grid_categories.MasterTableView.ClearEditItems();//to clear the EditMode 
                    grid_categories.EditIndexes.Clear();
                    grid_categories.MasterTableView.IsItemInserted = false;
                    grid_categories.Rebind();
                    break;
                default: break;
            }

        }


    }
}