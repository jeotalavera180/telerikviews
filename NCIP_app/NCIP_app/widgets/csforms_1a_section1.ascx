﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="csforms_1a_section1.ascx.cs" Inherits="NaplCom___Forms.csforms_1a_section1" %>



<telerik:RadPanelBar runat="server" ID="RadPanelBar1" Width="100%" Height="800px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Pre-charge investigation (PCI)" Expanded="true">
            <ContentTemplate>
                <ul>
                    <li>
                        <label>Complaint Source</label>
                        <br>
                        <label>Pls Check</label>
                    </li>
                    <li>
                        <telerik:RadComboBox ID="txt_complaintsource" runat="server" ShowToggleImage="false"
                            EmptyMessage="Choose" MarkFirstMatch="true" AllowCustomText="True" AppendDataBoundItems="True" EnableVirtualScrolling="True">
                            <Items>
                                <telerik:RadComboBoxItem Value="" Text="Please select" />
                            </Items>
                        </telerik:RadComboBox>
                    </li>
                    <li>

                        <telerik:RadGrid ID="grid_complainant" runat="server" OnNeedDataSource="grid_complainant_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_complainant_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Complainant"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>

                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="username"
                                        FilterControlAltText="Filter username column" HeaderText="Username"
                                        SortExpression="userName" UniqueName="userName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="password"
                                        FilterControlAltText="Filter password column" HeaderText="Password"
                                        SortExpression="password" UniqueName="password">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />

                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />

                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <table>
                                            <tr>
                                                <th>username</th>
                                                <th>password</th>
                                                <th>Action</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <telerik:RadComboBox ID="txt_username" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="txt_password" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" /></td>
                                            </tr>
                                        </table>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                    <%--Respondents--%>
                    <li>
                        <label>Rank</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_rank" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Surname, First Name, Middle Name</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_surnamefirstnamemiddlename" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Gender (M/F)</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_gender" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Contact No.</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_contactno2" runat="server"></telerik:RadGrid>
                    </li>
                    <%--Respondents End--%>
                    <li>
                        <label>Office/Unit Assignment</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_officeunitassignment" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Nature of Offense(s)</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_natureofoffense" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Specific Act(s)</label>
                    </li>
                    <li>
                        <telerik:RadGrid ID="grd_specificacts" runat="server"></telerik:RadGrid>
                    </li>
                    <li>
                        <label>Date Complaint Filed (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_complaintfiled" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>Name of Evaluator</label>
                    </li>
                    <li>
                        <telerik:RadComboBox ID="txt_nameofevaluator" runat="server"
                            EmptyMessage="Choose" MarkFirstMatch="true">
                        </telerik:RadComboBox>
                    </li>
                    <li>
                        <label>Date Evaluation terminated (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_evaluationterminated" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <%--Referred to other Disciplinary Authority/Office--%>
                    <li>
                        <label>PLEB (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_pleb" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>PNP (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_pnp" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>Others</label>
                        <br></br>
                        <label>(pls. specify)</label>
                    </li>
                    <li>
                        <telerik:RadComboBox ID="txt_others" runat="server"
                            EmptyMessage="Choose" MarkFirstMatch="true">
                        </telerik:RadComboBox>
                    </li>
                    <%--End Referred to other Disciplinary Authority/Office--%>
                    <li>
                        <label>PCI No.</label>
                    </li>
                    <li>
                        <telerik:RadComboBox ID="txt_pcino" runat="server"
                            EmptyMessage="Choose" MarkFirstMatch="true">
                        </telerik:RadComboBox>
                    </li>
                    <li>
                        <label>Date Complaint Assigned (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_complaintassigned" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>Name of Investigator(s)</label>
                    </li>
                    <li>
                        <telerik:RadComboBox ID="txt_nameofinvestigator" runat="server"
                            EmptyMessage="Choose" MarkFirstMatch="true">
                        </telerik:RadComboBox>
                    </li>
                    <li>
                        <label>Date PCI Report Submitted (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_pcireportsubmitted" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>Date Approved byHead of IMIS/R.O (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_approvedbyheadofimisro" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <%--Type of Disposition--%>
                    <li>
                        <label>Recommended for Dropping/Closure (Date Case Folder Forwarded to CO (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_recommendedfordroppingclosure" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                    <li>
                        <label>For SD Proceedings (Date Case Folder Forwarded to Docket Officer) (mm-dd-yyyy)</label>
                    </li>
                    <li>
                        <telerik:RadDatePicker ID="txt_forsdproceedings" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                        </telerik:RadDatePicker>
                    </li>
                </ul>
            </ContentTemplate>
        </telerik:RadPanelItem>

    </Items>
</telerik:RadPanelBar>
