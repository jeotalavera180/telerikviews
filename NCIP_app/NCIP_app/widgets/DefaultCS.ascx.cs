﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace NCIP_app.widgets
{

    public partial class DefaultCS : System.Web.UI.UserControl
    {
        protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                //get datasource if its the master table
                RadGrid1.DataSource = general.getSet("select * from t_User where isDeleted = 0");
            }
        }
        void Rebind()
        {
            RadGrid1.MasterTableView.ClearEditItems();//to clear the EditMode 
            RadGrid1.EditIndexes.Clear();
            RadGrid1.MasterTableView.IsItemInserted = false;
            RadGrid1.Rebind();
        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string primaryKey = "";
            try
            {
                //get the ownertableview current item row datakeyvalue
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex].GetDataKeyValue("id").ToString();
                //or this - prone to errors
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex]["id"].Text;
            }
            catch (Exception)
            {
            }

            /*
            * Allows on RowClick or ExpandCollapse to collapse all rows.
            **/
            //if (e.CommandName == "RowClick" || e.CommandName == "ExpandCollapse")
            //{
            //    bool lastState = e.Item.Expanded;

            //    if (e.CommandName == "ExpandCollapse")
            //    {
            //        lastState = !lastState;
            //    }

            //    CollapseAllRows();
            //    e.Item.Expanded = !lastState;
            //}

            Dictionary<object, object> values = null;
            LinkButton lb = null;
            GridEditFormItem editItem = null;
            e.Item.OwnerTableView.ShowHeader = true;
            switch (e.CommandName)
            {
                case "InitInsert":
                    e.Item.OwnerTableView.ShowHeader = false;
                    break;
                case "PerformInsert":
                case "Update":
                    values = new Dictionary<object, object>();
                    lb = (LinkButton)e.CommandSource;
                    //get the container
                    editItem = (GridEditFormItem)lb.NamingContainer;
                    //extract values - only works for GridEditFormItem
                    editItem.ExtractValues(values);
                    break;
                case "delete":
                    /*SqlCommand cmd = new SqlCommand("UPDATE t_matters set status='DELETED' where id=@id");
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = primaryKey;
                    general.performActionNoTrans(cmd);
                    RadGrid1.Rebind();*/
                    break;
                case "addCategory":
                    //get control values
                    RadComboBox username = (RadComboBox)e.Item.FindControl("txt_username");
                    RadComboBox password = (RadComboBox)e.Item.FindControl("txt_password");
                    var isActive = (RadButton)e.Item.FindControl("ckbx_isActive");
                    var trackSales = (RadButton)e.Item.FindControl("ckbx_trackAsSales");
                    //setup sqlcommand to execute
                    SqlCommand cmd = new SqlCommand("insert into t_User (username,password,department_id,isDeleted,role_id) values (@username,@password,@department_id,0,'E2195F7E-625B-4335-BC3D-E672079E48C6')");
                    cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username.Text;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password.Text;
                    cmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    bool isSaved = general.performAction(cmd);
                    if (isSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    Rebind();
                    break;
                case "updateCategory":
                    //get source of command - the control
                    Button btn = (Button)e.CommandSource;
                    //get the container
                    editItem =(GridEditFormItem)btn.NamingContainer;
                    //get controls
                    RadComboBox uword = (RadComboBox)editItem.FindControl("txt_username");
                    RadComboBox pword = (RadComboBox)editItem.FindControl("txt_password");
                    //setup sqlcmd to execute
                    SqlCommand editCmd = new SqlCommand("update t_User set username=@username,password=@password,department_id=@department_id,isDeleted=0,role_id='E2195F7E-625B-4335-BC3D-E672079E48C6' where id='" + primaryKey + "'");
                    editCmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = uword.Text;
                    editCmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = pword.Text;
                    editCmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    bool editisSaved = general.performAction(editCmd);
                    if (editisSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    Rebind();
                    break;
                case "deleteCategory":
                    //setup sqlcommand for setting isDeleted
                    SqlCommand cmd3 = new SqlCommand("UPDATE t_User set isDeleted=1 where id=@id");
                    cmd3.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = Guid.Parse(primaryKey);
                    general.performActionNoTrans(cmd3);
                    //bind back the RadGrid
                    Rebind();
                    break;
                case "addSubCategory":
                    //gets parent item
                    GridDataItem dataItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                    //get id
                    var userPrimaryKey = dataItem.GetDataKeyValue("id").ToString();
                    //get control values
                    RadComboBox title = (RadComboBox)e.Item.FindControl("txt_title");
                    RadComboBox desc = (RadComboBox)e.Item.FindControl("txt_description");
                    //setup sqlcommand to execute
                    cmd = new SqlCommand("insert into t_Content (title,description,user_id,isDeleted) values (@title,@description,@user_id,0)");
                    cmd.Parameters.Add("@title", System.Data.SqlDbType.VarChar).Value = title.Text;
                    cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar).Value = desc.Text;
                    cmd.Parameters.Add("@user_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse(userPrimaryKey);
                    isSaved = general.performAction(cmd);
                    if (isSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    else
                    {
                        //oh no
                    }
                    Rebind();
                    break;
                case "updateSubCategory":
                    //gets parent item
                    dataItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                    dataItem.ExtractValues(values);
                    //get id of parent item
                    userPrimaryKey = dataItem.GetDataKeyValue("id").ToString();
                    //get source of command - the controls
                    Button btn2 = (Button)e.CommandSource;
                    //get the container
                    editItem = (GridEditFormItem)btn2.NamingContainer;
                    //get control
                    title = (RadComboBox)editItem.FindControl("txt_title");
                    var description = (RadComboBox)editItem.FindControl("txt_description");
                    var tags = (RadComboBox)editItem.FindControl("txt_tags");
                    //setup sqlcmd to execute
                    editCmd = new SqlCommand("update t_Content set title=@title,description=@description,user_id=@user_id,tag=@tag,isDeleted=0 where id='" + primaryKey + "'");
                    editCmd.Parameters.Add("@title", System.Data.SqlDbType.VarChar).Value = title.Text;
                    editCmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar).Value = description.Text;
                    editCmd.Parameters.Add("@tag", System.Data.SqlDbType.VarChar).Value = tags.Text;
                    editCmd.Parameters.Add("@user_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse(userPrimaryKey);
                    editisSaved = general.performAction(editCmd);
                    if (editisSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    else
                    {
                        //oh no
                    }
                    Rebind();
                    break;
                default: break;
            }
        }

        //protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        //{
        //    if (e.Exception != null)
        //    {
        //        e.KeepInEditMode = true;
        //        e.ExceptionHandled = true;
        //    }
        //}

        private void CollapseAllRows()
        {
            foreach (GridItem item in RadGrid1.MasterTableView.Items)
            {
                item.Expanded = false;
            }
        }

        protected void RadGrid1_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            //every DetailTableView will need a datasource - where it gets its data from.
            switch (e.DetailTableView.Name)
            {
                case "sub-categories":
                    {
                        //gets id
                        string id = dataItem.GetDataKeyValue("id").ToString();
                        DataSet ds = general.getSet("SELECT * FROM t_Content WHERE user_id = '" + id + "'");
                        //add to the datasource
                        e.DetailTableView.DataSource = ds;
                        if (ds.Tables.Count > 0)
                        {
                            //if there is no rows
                            if (ds.Tables[0].Rows.Count <= 0)
                            {
                                //disable commanditemdisplay and header
                                e.DetailTableView.CommandItemDisplay = GridCommandItemDisplay.None;
                                e.DetailTableView.ShowHeader = false;
                            }
                            else
                            {
                                //enable
                                e.DetailTableView.CommandItemDisplay = GridCommandItemDisplay.Top;
                                e.DetailTableView.ShowHeader = true;
                            }
                        }
                        break;
                    }
            }
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //expands first item in mastertable
                //RadGrid1.MasterTableView.Items[0].Expanded = true;
                //and every child it has
                //RadGrid1.MasterTableView.Items[0].ChildItem.NestedTableViews[0].Items[0].Expanded = true;
            }
        }

        protected void RadGrid_Test_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //var s = RadGrid1.MasterTableView.EditFormSettings.FormTemplate;
            //RadGrid1.DataSource = general.getSet("select * from t_User where isDeleted = 0");
        }
        protected void RadGrid_Test_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }
        protected void RadGrid_Test_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string primaryKey = "";
            try
            {
                //get the ownertableview current item row datakeyvalue
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex].GetDataKeyValue("id").ToString();
                //or this - prone to errors
                primaryKey = e.Item.OwnerTableView.Items[e.Item.ItemIndex]["id"].Text;
            }
            catch (Exception)
            {
            }

            /*
            * Allows on RowClick or ExpandCollapse to collapse all rows.
            **/
            //if (e.CommandName == "RowClick" || e.CommandName == "ExpandCollapse")
            //{
            //    bool lastState = e.Item.Expanded;

            //    if (e.CommandName == "ExpandCollapse")
            //    {
            //        lastState = !lastState;
            //    }

            //    CollapseAllRows();
            //    e.Item.Expanded = !lastState;
            //}

            Dictionary<object, object> values = null;
            LinkButton lb = null;
            GridEditFormItem editItem = null;
            e.Item.OwnerTableView.ShowHeader = true;
            switch (e.CommandName)
            {
                case "InitInsert":
                    e.Item.OwnerTableView.ShowHeader = false;
                    break;
                case "PerformInsert":
                case "Update":
                    values = new Dictionary<object, object>();
                    lb = (LinkButton)e.CommandSource;
                    //get the container
                    editItem = (GridEditFormItem)lb.NamingContainer;
                    //extract values - only works for GridEditFormItem
                    editItem.ExtractValues(values);
                    break;
                case "delete":
                    /*SqlCommand cmd = new SqlCommand("UPDATE t_matters set status='DELETED' where id=@id");
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = primaryKey;
                    general.performActionNoTrans(cmd);
                    RadGrid1.Rebind();*/
                    break;
                case "addCategory":
                    //get control values
                    RadComboBox username = (RadComboBox)e.Item.FindControl("txt_username");
                    RadComboBox password = (RadComboBox)e.Item.FindControl("txt_password");
                    var isActive = (RadButton)e.Item.FindControl("ckbx_isActive");
                    var trackSales = (RadButton)e.Item.FindControl("ckbx_trackAsSales");
                    //setup sqlcommand to execute
                    SqlCommand cmd = new SqlCommand("insert into t_User (username,password,department_id,isDeleted,role_id) values (@username,@password,@department_id,0,'E2195F7E-625B-4335-BC3D-E672079E48C6')");
                    cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username.Text;
                    cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password.Text;
                    cmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    bool isSaved = general.performAction(cmd);
                    if (isSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    Rebind();
                    break;
                case "updateCategory":
                    //get source of command - the control
                    Button btn = (Button)e.CommandSource;
                    //get the container
                    editItem = (GridEditFormItem)btn.NamingContainer;
                    //get controls
                    RadComboBox uword = (RadComboBox)editItem.FindControl("txt_username");
                    RadComboBox pword = (RadComboBox)editItem.FindControl("txt_password");
                    //setup sqlcmd to execute
                    SqlCommand editCmd = new SqlCommand("update t_User set username=@username,password=@password,department_id=@department_id,isDeleted=0,role_id='E2195F7E-625B-4335-BC3D-E672079E48C6' where id='" + primaryKey + "'");
                    editCmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = uword.Text;
                    editCmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = pword.Text;
                    editCmd.Parameters.Add("@department_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse("a9a2d044-4204-45da-99b4-146f1da7a287");
                    bool editisSaved = general.performAction(editCmd);
                    if (editisSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    Rebind();
                    break;
                case "deleteCategory":
                    //setup sqlcommand for setting isDeleted
                    SqlCommand cmd3 = new SqlCommand("UPDATE t_User set isDeleted=1 where id=@id");
                    cmd3.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = Guid.Parse(primaryKey);
                    general.performActionNoTrans(cmd3);
                    //bind back the RadGrid
                    Rebind();
                    break;
                case "addSubCategory":
                    //gets parent item
                    GridDataItem dataItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                    //get id
                    var userPrimaryKey = dataItem.GetDataKeyValue("id").ToString();
                    //get control values
                    RadComboBox title = (RadComboBox)e.Item.FindControl("txt_title");
                    RadComboBox desc = (RadComboBox)e.Item.FindControl("txt_description");
                    //setup sqlcommand to execute
                    cmd = new SqlCommand("insert into t_Content (title,description,user_id,isDeleted) values (@title,@description,@user_id,0)");
                    cmd.Parameters.Add("@title", System.Data.SqlDbType.VarChar).Value = title.Text;
                    cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar).Value = desc.Text;
                    cmd.Parameters.Add("@user_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse(userPrimaryKey);
                    isSaved = general.performAction(cmd);
                    if (isSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    else
                    {
                        //oh no
                    }
                    Rebind();
                    break;
                case "updateSubCategory":
                    //gets parent item
                    dataItem = (GridDataItem)e.Item.OwnerTableView.ParentItem;
                    dataItem.ExtractValues(values);
                    //get id of parent item
                    userPrimaryKey = dataItem.GetDataKeyValue("id").ToString();
                    //get source of command - the controls
                    Button btn2 = (Button)e.CommandSource;
                    //get the container
                    editItem = (GridEditFormItem)btn2.NamingContainer;
                    //get control
                    title = (RadComboBox)editItem.FindControl("txt_title");
                    var description = (RadComboBox)editItem.FindControl("txt_description");
                    var tags = (RadComboBox)editItem.FindControl("txt_tags");
                    //setup sqlcmd to execute
                    editCmd = new SqlCommand("update t_Content set title=@title,description=@description,user_id=@user_id,tag=@tag,isDeleted=0 where id='" + primaryKey + "'");
                    editCmd.Parameters.Add("@title", System.Data.SqlDbType.VarChar).Value = title.Text;
                    editCmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar).Value = description.Text;
                    editCmd.Parameters.Add("@tag", System.Data.SqlDbType.VarChar).Value = tags.Text;
                    editCmd.Parameters.Add("@user_id", System.Data.SqlDbType.UniqueIdentifier).Value = Guid.Parse(userPrimaryKey);
                    editisSaved = general.performAction(editCmd);
                    if (editisSaved)
                    {
                        RadGrid1.Rebind();
                    }
                    else
                    {
                        //oh no
                    }
                    Rebind();
                    break;
                default: break;
            }
        }

        protected void RadGrid_Test_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {

        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode))
            {
                GridEditableItem editForm = (GridEditableItem)e.Item;
                RadGrid grid = (RadGrid)editForm.FindControl("RadGrid_Test");
                grid.DataSource = general.getSet("select * from t_User where isDeleted = 0");
            }

        }

        protected void RadGrid_Test_PreRender(object sender, EventArgs e)
        {

        }
    }
}