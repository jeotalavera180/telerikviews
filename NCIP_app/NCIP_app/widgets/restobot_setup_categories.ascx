﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_categories.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_categories" %>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script src="../js/grid.js" type="text/javascript"></script>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        var radWindow1 = null;
        var radGrid1 = null;
        var panelStep1 = null;
        var panelStep2 = null;

        function pageLoad() {
            radGrid1 = $find("<%= grid_categories.ClientID %>");
            radWindow1 = $find("<%= RadWindow1.ClientID %>");
            panelStep1 = $get("<%= FirstStepPanel.ClientID %>");
            panelStep2 = $get("<%= SecondStepPanel.ClientID %>");
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BookNowCloseButton">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grid_categories" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grid_categories">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grid_categories" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<h1>Categories</h1>
<telerik:RadGrid ID="grid_categories" runat="server" OnNeedDataSource="grid_categories_NeedDataSource"
    AllowPaging="True" AllowSorting="True" OnItemCommand="grid_categories_ItemCommand"
    CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
    PageSize="10">
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
        DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
        <HeaderStyle Width="102px" />
        <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Create Category"
            ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
        <Columns>
            <telerik:GridBoundColumn DataField="id" Display="False"
                UniqueName="id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="username"
                HeaderText="CATEGORY"
                SortExpression="category" UniqueName="category">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn
                ConfirmTitle="Add" CommandName="newInsert" CommandArgument="id"
                ButtonCssClass="icon-plus-sign" />
            <telerik:GridButtonColumn
                ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                ButtonCssClass="icon-edit" />
            <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                ButtonCssClass="icon-trash" />
        </Columns>
        <%--Nested--%>
        <NestedViewTemplate>
            <telerik:RadGrid ID="grid_categories2" runat="server" OnNeedDataSource="grid_categories_NeedDataSource"
                AllowPaging="True" AllowSorting="True" OnItemCommand="grid_categories_ItemCommand"
                CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                PageSize="10">
                <ClientSettings>
                    <Selecting AllowRowSelect="True" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                    DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                    <HeaderStyle Width="102px" />
                    <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Create Sub-Category"
                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn DataField="id" Display="False"
                            UniqueName="id">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="username"
                            HeaderText="SUB-CATEGORY"
                            SortExpression="category" UniqueName="category">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn
                            ConfirmTitle="Add" CommandName="newInsert" CommandArgument="id"
                            ButtonCssClass="icon-plus-sign" />
                        <telerik:GridButtonColumn
                            ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                            ButtonCssClass="icon-edit" />
                        <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                            ButtonCssClass="icon-trash" />
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <div class="carBackground">
                <div style="float: left;">
                    <asp:Image ID="CarImage" runat="server" AlternateText="Car Image" />
                </div>
                <div style="float: right; width: 50%">
                    <div class="carTitle">
                        bname
                        model
                        <span style="color: #555555; float: right; font-size: 14px; font-weight: normal;">Rented
                                        rentcount
                                        times</span>
                    </div>
                    <hr class="lineSeparator" />
                    <table width="100%" class="carInfo">
                        <tr>
                            <td>
                                <strong>Year:</strong>
                                year
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Classification:</strong>
                                class
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Transmission:</strong>
                                trans
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Fuel Type:</strong>
                                fuel
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Price:</strong> &#8364;price
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both">
                </div>
            </div>
            <%--<telerik:RadGrid ID="grid_subCategories" runat="server" OnNeedDataSource="grid_subCategories_NeedDataSource"
                AllowPaging="True" AllowSorting="True" OnItemCommand="grid_subCategories_ItemCommand"
                CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                PageSize="10">

                <ClientSettings>
                    <Selecting AllowRowSelect="True" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>

                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                    DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                    <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Create Sub-Category"
                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                    <Columns>
                        <telerik:GridBoundColumn DataField="id" Display="False"
                            UniqueName="id">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="subCategory"
                            HeaderText="Sub-Category"
                            SortExpression="subCategory" UniqueName="subCategory">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn
                            ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                            ButtonCssClass="icon-edit" />
                        <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                            ButtonCssClass="icon-trash" />
                    </Columns>
                    <EditFormSettings EditFormType="Template">
                        <FormTemplate>
                            <center>
                                <ul>
                                <li>
                                    <label>Name</label>
                                    <telerik:RadComboBox ID="txt_subCategoryName" runat="server" ShowToggleImage="false"
                                        EmptyMessage="Choose" AllowCustomText="True">
                                    </telerik:RadComboBox>
                                </li>
                                <li>
                                    <label>Description</label>
                                    <telerik:RadComboBox ID="txt_subCategoryDescription" runat="server" ShowToggleImage="false"
                                        EmptyMessage="Choose" AllowCustomText="True">
                                    </telerik:RadComboBox>
                                </li>
                                <li>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <asp:Button ID="btn_subCategorySave" runat="server" Text="Submit" CommandName="newInsert" />
                                    <asp:Button ID="btn_subCategoryCancel" runat="server" Text="Cancel" CommandName="cancel" />
                                </li>
                            </ul>
                                </center>
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings EnableRowHoverStyle="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="" />
                </ClientSettings>
                <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>--%>
        </NestedViewTemplate>
        <%--Nested--%>
        <EditFormSettings EditFormType="Template" PopUpSettings-Modal="true">
            <FormTemplate>
                <center>
                <ul>
                    <li>
                        <label>Name</label>
                        <telerik:RadComboBox ID="txt_name" runat="server" ShowToggleImage="false"
                            EmptyMessage="Choose" AllowCustomText="True">
                        </telerik:RadComboBox>
                    </li>
                    <li>
                        <label>Description</label>
                        <telerik:RadComboBox ID="txt_description" runat="server" ShowToggleImage="false"
                            EmptyMessage="Choose" AllowCustomText="True">
                        </telerik:RadComboBox>
                    </li>
                    <li>
                        <table>
                            <tr>
                                <td>
                                    <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                </td>
                                <td>
                                    <input type="checkbox" name="ckbx_trackSales" value="">Track as Sales<br>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <li>
                        <asp:Button ID="btn_Save" runat="server" Text="Save" CommandName="newInsert" />
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="cancel" />
                    </li>
                </ul>
                    </center>
            </FormTemplate>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <ClientSettings EnableRowHoverStyle="true" EnablePostBackOnRowClick="true">
        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
        <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="" />
    </ClientSettings>
    <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>

</telerik:RadGrid>
<div>
    <telerik:RadWindow ID="RadWindow1" runat="server" VisibleTitlebar="false" Modal="true" AutoSize="true"
        Behaviors="None" VisibleStatusbar="false">
        <ContentTemplate>
            <asp:Panel ID="FirstStepPanel" runat="server">
                <div class="bookNowFrame">
                    <div class="bookNowTitle">
                        Fill in the form to make your reservation
                    </div>
                    <hr class="lineSeparator" style="margin: 12px 0 12px 0" />
                    <table cellspacing="8">
                        <colgroup>
                            <col width="90px" />
                            <col width="150px" />
                            <col />
                            <col />
                        </colgroup>
                        <tr>
                            <td>DATE OF RENT
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="DateOfRentPicker" Width="130px" runat="server" />
                            </td>
                            <td>RETURN DATE
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="ReturnDatePicker" Width="130px" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <hr class="lineSeparator" style="margin: 12px 0 12px 0" />
                    <table cellspacing="8">
                        <colgroup>
                            <col width="90px" />
                            <col />
                        </colgroup>
                        <tr>
                            <td>FIRST NAME
                            </td>
                            <td>
                                <telerik:RadTextBox ID="FirstNameTextBox" runat="server" Width="190px" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>LAST NAME
                            </td>
                            <td>
                                <telerik:RadTextBox ID="LastNameTextBox" runat="server" Width="190px" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>EMAIL
                            </td>
                            <td>
                                <telerik:RadTextBox ID="EmailTextBox" runat="server" Width="190px" /><br />
                            </td>
                        </tr>
                    </table>
                    <hr class="lineSeparator" style="margin: 12px 0 15px 0" />
                    <telerik:RadButton ID="BookNowButton" runat="server" Text="Book Now"
                        Width="100px" OnClientClicking="bookNowClicking" UseSubmitBehavior="false" />
                    <telerik:RadButton ID="CancelButton" runat="server" Text="Cancel"
                        Width="100px" OnClientClicking="cancelClicking" UseSubmitBehavior="false" />
                </div>
            </asp:Panel>
            <asp:Panel ID="SecondStepPanel" runat="server" Style="display: none; padding: 120px 20px 0 30px; width: 480px;">
                <div style="float: left;">
                    <img src="Images/Confirmation.png" alt="Confirmation sign" />
                </div>
                <div style="float: left; padding: 10px 0 0 20px;">
                    <div class="bookNowComplete">
                        You have successfully made your reservation!
                    </div>
                    <hr class="lineSeparator" style="margin: 10px 10px 20px 0" />
                    <telerik:RadButton ID="BookNowCloseButton" runat="server" Text="Close"
                        Width="100px" OnClientClicking="bookNowCloseClicking" UseSubmitBehavior="false" />
                </div>
                <div style="clear: both">
                </div>
            </asp:Panel>
        </ContentTemplate>
    </telerik:RadWindow>
</div>
