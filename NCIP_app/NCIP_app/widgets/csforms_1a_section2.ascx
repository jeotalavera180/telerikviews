﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="csforms_1a_section2.ascx.cs" Inherits="NaplCom___Forms.csforms_1a_section2" %>
<telerik:radpanelbar runat="server" id="RadPanelBar1" width="750" height="250"
    skin="Telerik" expandmode="FullExpandedItem" onclientload="onLoad">
            <Items>

<telerik:RadPanelItem Text="Complaints for dropping/closure" Expanded="true">
                    <ContentTemplate>
                        <ul>
                            <li>
                                <label>Date Case Folder Received at Central Records (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderreceivedatcentralrecords" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderreceivedatcentralrecords_SelectedDateChanged">
                                <Calendar ID="Calendar2" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Case Folder Received at LAS (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderreceivedatlas" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderreceivedatlas_SelectedDateChanged">
                                <Calendar ID="Calendar1" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Case Folder Received by the Evaluating Officer (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderreceivedbytheevaluatingofficer" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderreceivedbytheevaluatingofficer_SelectedDateChanged">
                                <Calendar ID="Calendar3" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Draft Decision/Resolution forwarded by LAS to the Commission En Banc (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_draftdecisionresolutionforwardedbylas" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_draftdecisionresolutionforwardedbylas_SelectedDateChanged">
                                <Calendar ID="Calendar4" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Case Folder Received at LAS (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderreceivedatlas2" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderreceivedatlas2_SelectedDateChanged">
                                <Calendar ID="Calendar5" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OCPNP</label>
                           </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_ocpnp" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_ocpnp_SelectedDateChanged">
                                <Calendar ID="Calendar6" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OCCDG</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_occdg" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_occdg_SelectedDateChanged">
                                <Calendar ID="Calendar7" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OCASU</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_ocasu" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_ocasu_SelectedDateChanged">
                                <Calendar ID="Calendar8" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OCLTP</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_ocltp" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_ocltp_SelectedDateChanged">
                                <Calendar ID="Calendar9" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OVCEO</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_ovceo" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_ovceo_SelectedDateChanged">
                                <Calendar ID="Calendar10" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>OC/OSILG</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_ocosilg" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_ocosilg_SelectedDateChanged">
                                <Calendar ID="Calendar11" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                        </ul>
                    </ContentTemplate>
                </telerik:RadPanelItem>
                

            </Items>
        </telerik:radpanelbar>
