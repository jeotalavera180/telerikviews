﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="csforms_1a_section3.ascx.cs" Inherits="NaplCom___Forms.csforms_1a_section3" %>
<telerik:radpanelbar runat="server" id="RadPanelBar1" width="750" height="250"
    skin="Telerik" expandmode="FullExpandedItem" onclientload="onLoad">
            <Items>


<telerik:RadPanelItem Text="Complaints for summary dismissal proceedings" Expanded="true">
                    <ContentTemplate>
                        <ul>
                            <li>
                                <label>Date Case Docketed (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casedocketed" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casedocketed_SelectedDateChanged">
                                <Calendar ID="Calendar10" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>SD Case No.</label>
                            </li>
                            <li>
                                <telerik:RadComboBox ID="txt_sdcaseno" runat="server" 
                                    EmptyMessage="Choose" MarkFirstMatch="true">
                                </telerik:RadComboBox>
                            </li>
                            <li>
                                <label>Date S.O of SHO issued (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_soofshoissued" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_soofshoissued_SelectedDateChanged">
                                <Calendar ID="Calendar1" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Name of Summary Hearing Officer(s)</label>
                            </li>
                            <li>
                                <telerik:RadComboBox ID="txt_nameofsummaryhearing" runat="server" 
                                    EmptyMessage="Choose" MarkFirstMatch="true">
                                </telerik:RadComboBox>
                            </li>
                            <li>
                                <label>Date Case Folder Received By SHO (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderreceivedbysho" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderreceivedbysho_SelectedDateChanged">
                                <Calendar ID="Calendar2" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date of Issuance of Summons (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_issuanceofsummons" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_issuanceofsummons_SelectedDateChanged">
                                <Calendar ID="Calendar3" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date of Pre-Hearing Conference (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_prehearingconference" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_prehearingconference_SelectedDateChanged">
                                <Calendar ID="Calendar4" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <%--In Case of Motion for Preventive Suspension--%>
                            <li>
                                <label>Date Filed (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_filed" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_filed_SelectedDateChanged">
                                <Calendar ID="Calendar5" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Resolved (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_resolved" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_resolved_SelectedDateChanged">
                                <Calendar ID="Calendar6" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <%--End - In Case of Motion for Preventive Suspension--%>

                            <%--In Case of Motion for transfer of Venue--%>
                            <li>
                                <label>Date Filed (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_filed2" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_filed2_SelectedDateChanged">
                                <Calendar ID="Calendar8" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Resolved (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_resolved2" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_resolved2_SelectedDateChanged">
                                <Calendar ID="Calendar7" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <%--End - In Case of Motion for transfer of Venue--%>

                             <%--In Case of Motion for Inhibition--%>
                            <li>
                                <label>Date Filed (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_filed3" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_filed3_SelectedDateChanged">
                                <Calendar ID="Calendar9" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Resolved (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_resolved3" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_resolved3_SelectedDateChanged">
                                <Calendar ID="Calendar11" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <%--End - In Case of Motion for Inhibition--%>
                            <li>
                                <label>Date of Submission of Position Paper (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_submissionofpositionpaper" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_submissionofpositionpaper_SelectedDateChanged">
                                <Calendar ID="Calendar12" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date of Clarificatory Meeting (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_clarificatorymeeting" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_clarificatorymeeting_SelectedDateChanged">
                                <Calendar ID="Calendar13" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date ROI Submitted to the Office of the Director (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_roisubmittedtotheofficeofthedirector" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_roisubmittedtotheofficeofthedirector_SelectedDateChanged">
                                <Calendar ID="Calendar14" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>
                            <li>
                                <label>Date Case Folder Indorsed to LAS (mm-dd-yyyy)</label>
                            </li>
                            <li>
                                 <telerik:RadDatePicker ID="txt_casefolderindorsedtolas" runat="server" MinDate="1900-01-01" AutoPostBack="true"
                                OnSelectedDateChanged="txt_casefolderindorsedtolas_SelectedDateChanged">
                                <Calendar ID="Calendar15" RangeMinDate="1900-01-01" runat="server">
                                </Calendar>
                                </telerik:RadDatePicker>
                            </li>

                        </ul>
                    </ContentTemplate>
                </telerik:RadPanelItem>
                

            </Items>
        </telerik:radpanelbar>
