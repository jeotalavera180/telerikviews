﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_sales.ascx.cs" Inherits="NCIP_app.widgets.restobot_sales" %>


<telerik:RadPanelBar runat="server" ID="RadPanelBar_Details" Width="100%" Height="100%"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Details" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="txt_branch">Branch</label>
                            <telerik:RadComboBox ID="txt_branch" runat="server"
                                EmptyMessage="Choose" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="txt_date">Date</label>
                            <telerik:RadDatePicker ID="txt_date" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <label for="txt_shift">Shift</label>
                            <telerik:RadComboBox ID="txt_shift" runat="server"
                                EmptyMessage="Choose" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>


<telerik:RadPanelBar runat="server" ID="RadPanelBar_Statistics" Width="100%" Height="280px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Statistics" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="numtxt_customerCount">Customer Count</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_customerCount" Width="250px" Value="1" 
                                EmptyMessage="Enter Customer Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="numtxt_transactionCount">Transaction Count</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_transactionCount" Width="250px" Value="1" 
                                EmptyMessage="Enter Transaction Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                        </tr>
                    <tr>
                        <td>
                            <label for="numtxt_deliveryTransactionCount">Delivery Transaction Count</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_deliveryTransactionCount" Width="250px" Value="1" 
                                EmptyMessage="Enter Delivery Transaction Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="numtxt_creditCardCount">Credit Card Count</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_creditCardCount" Width="250px" Value="1" 
                                EmptyMessage="Enter Credit Card Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label for="numtxt_firstTimeGuest">First Time Guest</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_firstTimeGuest" Width="250px" Value="1" 
                                EmptyMessage="Enter First Time Guest Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="numtxt_repeatGuest">Repeat Guest</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_repeatGuest" Width="250px" Value="1" 
                                EmptyMessage="Enter Repeat Guest Count" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>


<telerik:RadPanelBar runat="server" ID="RadPanelBar_noneSalesMiscellaneous" Width="100%" Height="150px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Non Sales Miscellaneous" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="numtxt_vat">VAT</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_vat" Width="250px" Value="1" 
                                EmptyMessage="Enter VAT" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="numtxt_serviceCharge">Service Charge</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_serviceCharge" Width="250px" Value="1" 
                                EmptyMessage="Enter Service Charge" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="2">
                            </telerik:RadNumericTextBox>
                        </td>
                  </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>


<telerik:RadPanelBar runat="server" ID="RadPanelBar_salesBySettlements" Width="100%" Height="600px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Sales By Settlements" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="">Credit Card Sales</label>
                            <telerik:RadComboBox ID="txt_creditCardSales" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">Cash in Drawer</label>
                            <telerik:RadComboBox ID="txt_cashInDrawer" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">GC Redeemed</label>
                            <telerik:RadComboBox ID="txt_gcRedeemed" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">Delivery Sales</label>
                            <telerik:RadComboBox ID="txt_deliverySales" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 90</label>
                            <telerik:RadComboBox ID="txt_comp90" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">COMP 91</label>
                            <telerik:RadComboBox ID="txt_comp91" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 92</label>
                            <telerik:RadComboBox ID="txt_comp92" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">COMP 93</label>
                            <telerik:RadComboBox ID="txt_comp93" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 94</label>
                            <telerik:RadComboBox ID="txt_comp94" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">COMP 95</label>
                            <telerik:RadComboBox ID="txt_comp95" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 96</label>
                            <telerik:RadComboBox ID="txt_comp96" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">COMP 97</label>
                            <telerik:RadComboBox ID="txt_comp97" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 98</label>
                            <telerik:RadComboBox ID="txt_comp98" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">COMP 99</label>
                            <telerik:RadComboBox ID="txt_comp99" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">COMP 100</label>
                            <telerik:RadComboBox ID="txt_comp100" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <label for="">SEN CIT DISC</label>
                            <telerik:RadComboBox ID="txt_senCitDisc" runat="server"
                                EmptyMessage="" MarkFirstMatch="true">
                            </telerik:RadComboBox>
                        </td>
                  </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>



<telerik:RadPanelBar runat="server" ID="RadPanelBar_cashForDeposit" Width="100%" Height="150px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Cash For Deposit" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="">GC Sales</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_gcSales" Width="250px" Value="1" 
                                EmptyMessage="Enter VAT" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="2">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="">Other Income</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_otherIncome" Width="250px" Value="1" 
                                EmptyMessage="Enter Service Charge" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="2">
                            </telerik:RadNumericTextBox>
                        </td>
                  </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>


<telerik:RadPanelBar runat="server" ID="RadPanelBar_salesByCategory" Width="100%" Height="200px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Cash For Deposit" Expanded="true">
            <ContentTemplate>
                <center>
                <table>
                    <tr>
                        <td>
                            <label for="">BEER</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_beer" Width="250px" Value="1" 
                                EmptyMessage="" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="1">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="">LIQUOR</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_liquor" Width="250px" Value="1" 
                                EmptyMessage="" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="1">
                            </telerik:RadNumericTextBox>
                        </td>
                  </tr>
                    <tr>
                        <td>
                            <label for="">FOOD</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_food" Width="250px" Value="1" 
                                EmptyMessage="" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="1">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <label for="">BEVERAGE</label>
                            <telerik:RadNumericTextBox runat="server" ID="numtxt_beverage" Width="250px" Value="1" 
                                EmptyMessage="" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="1">
                            </telerik:RadNumericTextBox>
                        </td>
                  </tr>
                </table>
                    </center>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>