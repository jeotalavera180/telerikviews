﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_purchases.ascx.cs" Inherits="NCIP_app.widgets.restobot_purchases" %>



<telerik:RadPanelBar runat="server" ID="RadPanel" Width="100%" Height="800px"
    ExpandMode="FullExpandedItem">
    <Items>
        <telerik:RadPanelItem Text="Purchases" Expanded="true">
            <ContentTemplate>
                <ul>
                    <li>
                        <telerik:RadGrid ID="grid_purchases" runat="server" OnNeedDataSource="grid_purchases_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_purchases_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Purchases"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="invoiceNo"
                                        FilterControlAltText="Filter Invoice No. column" HeaderText="Invoice Number"
                                        SortExpression="invoiceNo" UniqueName="invoiceNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="purchaseDate"
                                        FilterControlAltText="Filter Purchase Date column" HeaderText="Purchase Date"
                                        SortExpression="purchaseDate" UniqueName="purchaseDate">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="branch"
                                        FilterControlAltText="Filter branch column" HeaderText="Branch"
                                        SortExpression="branch" UniqueName="branch">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="supplier"
                                        FilterControlAltText="Filter Supplier column" HeaderText="Supplier"
                                        SortExpression="supplier" UniqueName="supplier">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="netAmount"
                                        FilterControlAltText="Filter Net Amount column" HeaderText="Net Amount"
                                        SortExpression="netAmount" UniqueName="netAmount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="vatAmount"
                                        FilterControlAltText="Filter Net Amount column" HeaderText="Vat Amount"
                                        SortExpression="vatAmount" UniqueName="vatAmount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="totalAmount"
                                        FilterControlAltText="Filter Net Amount column" HeaderText="Total Amount"
                                        SortExpression="totalAmount" UniqueName="totalAmount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />
                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />
                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <table>
                                            <tr>
                                                <th>Branch</th>
                                                <th>Purchase Date</th>
                                                <th>Invoice Number</th>
                                                <th>Supplier</th>
                                                <th>Action</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <telerik:RadComboBox ID="txt_branch" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="txt_purchaseDate" runat="server" MinDate="1900-01-01" AutoPostBack="false">
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="txt_invoiceNumber" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="txt_supplier" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" /></td>
                                            </tr>
                                        </table>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                </ul>
            </ContentTemplate>
        </telerik:RadPanelItem>
    </Items>
</telerik:RadPanelBar>
