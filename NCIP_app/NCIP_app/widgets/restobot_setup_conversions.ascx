﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_setup_conversions.ascx.cs" Inherits="NCIP_app.widgets.restobot_setup_conversions" %>


<telerik:RadPanelItem Text="Conversions" Expanded="true">
    <contenttemplate>
                <ul>
                    <li>
                        <telerik:RadGrid ID="grid_conversions" runat="server" OnNeedDataSource="grid_conversions_NeedDataSource"
                            AllowPaging="True" AllowSorting="True" OnItemCommand="grid_conversions_ItemCommand"
                            CellSpacing="0" GridLines="None" ShowFooter="True" ShowStatusBar="True"
                            PageSize="10">

                            <ClientSettings>
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>

                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False"
                                DataKeyNames="id" TableLayout="Fixed" AllowAutomaticUpdates="false" AllowAutomaticInserts="false">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add Conversion"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowRefreshButton="true"></CommandItemSettings>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="id" Display="False"
                                        FilterControlAltText="Filter id column" UniqueName="id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="fromUnit"
                                         HeaderText="From Unit"
                                        SortExpression="fromUnit" UniqueName="fromUnit">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="toUnit"
                                        HeaderText="To Unit"
                                        SortExpression="toUnit" UniqueName="toUnit">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="factor"
                                     HeaderText="Factor"
                                        SortExpression="factor" UniqueName="factor">
                                    </telerik:GridBoundColumn>
                                    
                                    <telerik:GridButtonColumn
                                        ConfirmTitle="Update" CommandName="update" CommandArgument="id"
                                        ButtonCssClass="icon-edit" />
                                    <telerik:GridButtonColumn ConfirmText="Delete?" ConfirmDialogType="RadWindow"
                                        ConfirmTitle="delete" CommandName="delete" CommandArgument="id"
                                        ButtonCssClass="icon-trash" />
                                </Columns>

                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <ul>
                                                <li>
                                                <label>From</label>
                                                </li>
                                                <li>

                                                    <telerik:RadComboBox ID="txt_from" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>To</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_to" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                <li>
                                                 <label>Factor</label>
                                                </li>
                                                <li>
                                                    <telerik:RadComboBox ID="txt_factor" runat="server" ShowToggleImage="false"
                                                        EmptyMessage="Choose" AllowCustomText="True">
                                                    </telerik:RadComboBox>
                                                </li>
                                                
                                                <li>
                                                    <input type="checkbox" name="ckbx_isActive" value="active">Is Active<br>
                                                    <input type="checkbox" name="ckbx_trackSales" value="Bike">Track as sales<br>
                                                    <asp:Button ID="Button1" runat="server" Text="Save" CommandName="newInsert" />
                                                </li>
                                        </ul>
                                    </FormTemplate>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <ClientSettings EnableRowHoverStyle="true"></ClientSettings>
                            <PagerStyle Mode="NextPrevAndNumeric" PagerTextFormat="{4} Page {0} from {1}, rows {2} to {3} from {5}"></PagerStyle>
                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                        </telerik:RadGrid>
                    </li>
                </ul>
            </contenttemplate>
</telerik:RadPanelItem>