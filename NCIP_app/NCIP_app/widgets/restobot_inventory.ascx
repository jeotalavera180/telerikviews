﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="restobot_inventory.ascx.cs" Inherits="NCIP_app.widgets.restobot_inventory" %>

<telerik:RadPanelItem Text="Purchases" Expanded="true">
    <contenttemplate>
<div>
    <label>Branch</label>
    <telerik:RadComboBox ID="txt_branch" runat="server" ShowToggleImage="false"
    EmptyMessage="Choose" AllowCustomText="True">
    </telerik:RadComboBox>
</div>
<div>
    <label>Entry Date</label>
    <telerik:RadDatePicker ID="txt_purchaseDate" runat="server" MinDate="1900-01-01" AutoPostBack="false">
    </telerik:RadDatePicker>    
</div>
    </contenttemplate>
</telerik:RadPanelItem>