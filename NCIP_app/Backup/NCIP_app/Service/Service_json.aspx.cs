﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using NCIP_app.Service;


public partial class Service_Service_json : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            try
            {
                //get all controls from the page
                int TotalControls = Request.Form.AllKeys.Length;
                //make sure they are valid keys and not hidden field values
                string action = (string)Request.Form["action"] ?? "allCases";
                //use reflction to get the specific method
                Type t = typeof(NCIP_be);
                MethodInfo method = t.GetMethod(action);
                string p1 = Request.Form["param1"] ?? "";
                string p2 = Request.Form["param2"] ?? "";
                string p3 = Request.Form["param3"] ?? "";

                string results = (string)method.Invoke(null, new object[] { new string[] { 
                        p1,
                        p2,
                        p3
                    } });
                Response.Write("[" + results + "]");
            }
            catch (Exception)
            {
                Response.Write("INVALID REQUEST");
            }

        }
    }
}