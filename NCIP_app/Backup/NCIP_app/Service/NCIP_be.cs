﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace NCIP_app.Service
{
    public class NCIP_be
    {
        public static string none(params object[] nothing)
        {
            return "No Access";
        }
        public static String getCases(params object[] strings)
        {
            return "ats";
        }
        public static string allCases(params object[] strings)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select *,'' as relativeTime from t_cases");
                DataSet ds = general.getSet(cmd);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["fillingDate"].ToString() != "")
                    {
                        dr["relativeTime"] = general.RelativeDate((DateTime)dr["fillingDate"]);
                    }
                }
                return general.DStoJSON(ds);
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string allMatters(params object[] strings)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select * from t_matter");
                DataSet ds = general.getSet(cmd);
                return general.DStoJSON(ds);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
