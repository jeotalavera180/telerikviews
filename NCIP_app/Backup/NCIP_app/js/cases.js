﻿function show(match_found) {
    var element = document.getElementById(match_found);
    element.style.display = "";
}

function hide(match_found) {
    var element = document.getElementById(match_found);
    element.style.display = "none";
}
function show(advanced_search) {
    var element = document.getElementById(advanced_search);
    element.style.display = "";
}
function hide(advanced_search) {
    var element = document.getElementById(advanced_search);
    element.style.display = "none";
}
function show(advanced) {
    var element = document.getElementById(advanced);
    element.style.display = "";
}
function hide(advanced) {
    var element = document.getElementById(advanced);
    element.style.display = "none";
}
function show(basic) {
    var element = document.getElementById(basic);
    element.style.display = "";
}
function hide(basic) {
    var element = document.getElementById(basic);
    element.style.display = "none";
}
function show(search_box) {
    var element = document.getElementById(search_box);
    element.style.display = "";
}
function hide(search_box) {
    var element = document.getElementById(search_box);
    element.style.display = "none";
}
var pageNum = 1;
jQuery(document).ready(function ($) {

    jQuery.ajax({
        type: 'POST',
        url: "/Service/Service_json.aspx",
        data: { action: 'allCases', filter: '', page: jQuery("#pageNum").html() },
        success: function (data) {
            //convert data to json
            var out = '';
            var json = jQuery.parseJSON(data);
            for (i = 0; i < json.length; i++) {
                out += '' +
                               ' <tr> ' +
                               ' <td data-title="Case No." class="col-2"><i class="icon-flag-default"></i> <a href="caseprofile/' + json[i].runningNum + '">' + json[i].caseNum + '</a></td>' +
                               ' <td data-title="Case Title" class="col-3">' + json[i].caseNum + '</td>' +
                               ' <td data-title="Start Date" class="col-4">' + json[i].relativeTime + '</td>' +
                               ' <td data-title="Division/ Handling Lawyer" class="col-5">' + json[i].region + '<br>' + json[i].hearingOfficer + '</td>' +
                               ' <td data-title="Case Status" class="col-6">' + json[i].recordStatus + '</td>' +
                               ' <td class="col-7"><i class="icon-edit"></i> <i class="icon-trash"></i></td>' +
                               ' </tr>';
                ;
            }
            jQuery("#pageNum").html(pageNum);
            jQuery("#totalpage").html("Displaying 1 of " + json.length + " cases");
            jQuery("#tbodyContentTable").html(out);
            /*
            jQuery("#tbodyContentTable");
                   
            */
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            errorMsg("Invalid Credentials, please try again");
            jQuery("#ats_username").focus();
        }
    });

    $('#sidebar-arrow').click(function () {

        if ($(this).find('i').hasClass('icon-chevron-right')) {
            $('.sidebar-wrapper').animate({ left: 0 }, function () {
                $(this).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
            });
        } else {
            $('.sidebar-wrapper').animate({ left: -93 }, function () {
                $(this).find('i').addClass('icon-chevron-right').removeClass('icon-chevron-left');
            });
        }
    });

    $("#opener").click(function () {
        TINY.box.show({
            html: private_message_form,
            width: 350,
            height: 288,
            fixed: true
        });
    });
});