﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NCIP_app
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string defaultPage = "blank";
            Control myUserControlObject = LoadControl("~/widgets/" + defaultPage + ".ascx") as Control;
            if (Page.RouteData.Values["page"] != null)
            {
                defaultPage = (string)Page.RouteData.Values["page"];
            }
            try
            {
                myUserControlObject = LoadControl("~/widgets/" + defaultPage + ".ascx") as Control;
            }
            catch (Exception ex)
            {
                txter.Text = ex.ToString();
            }
            PlaceHolder1.Controls.Add(myUserControlObject);
        }
    }
}