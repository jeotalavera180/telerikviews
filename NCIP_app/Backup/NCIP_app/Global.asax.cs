﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;

namespace NCIP_app
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(System.Web.Routing.RouteTable.Routes);

        }

        void RegisterRoutes(System.Web.Routing.RouteCollection routes)
        {
            routes.MapPageRoute("homeSignIn", "login", "~/default.aspx");
            routes.MapPageRoute("login1vars", "login/{id}", "~/default.aspx");
            routes.MapPageRoute("home1vars", "{page}", "~/index.aspx");
            routes.MapPageRoute("home2vars", "{page}/{id}", "~/index.aspx");
            routes.MapPageRoute("home4vars", "{page}/{id}/{name}/{term}", "~/index.aspx");
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
